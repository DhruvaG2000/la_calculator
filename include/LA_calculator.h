/*
 * =====================================================================================
 *
 *       Filename:  LA_calculator.h
 *
 *    Description:  General purpose Eigen based Linear algebra calculator header file
 *
 *        Version:  1.0
 *        Created:  09/13/2021 02:21:08 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Dhruva Gole (181030017), goledhruva@gmail.com
 *   Organization:  VJTI
 *
 * =====================================================================================
 */

#include <iostream>
#include <stdio.h>
#include <vector>
#include "Eigen/Dense"
#include <stdlib.h>


class Calculator
{
	int r, c;
public:
	Calculator() {
		r = 3;
		c = 3;
	};
	Calculator( int arg ) {
		printf("STILL TODO\n");
	}
	int Inverse();
	int Determinant();
	int DMAS();
	~Calculator(){};
};
