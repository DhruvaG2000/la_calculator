/*
 * =====================================================================================
 *
 *       Filename:  main.cpp
 *
 *    Description: main part of this project
 *
 *        Version:  1.0
 *        Created:  09/13/2021 02:20:46 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Dhruva Gole (181030017), goledhruva@gmail.com
 *   Organization:  VJTI
 *
 * =====================================================================================
 */

#include "../include/LA_calculator.h"

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  Starting point of this program
 * =====================================================================================
 */
	int
main ( int argc, char *argv[] )
{
	int choice;
	Calculator obj;
	if (argc >= 2) {
		printf("Using default Matrices\n");
		Calculator obj(1);
	}
	else {
		Calculator obj;
	}
	printf("Choose from below:\n");
	printf("1. Determinant\n2. Inverse\n3. DMAS\n4. TODO...\n");
	std::cin >> choice;
	switch(choice)
	{
		case 1: obj.Determinant();
				break;
		case 2: obj.Inverse();
				break;
		case 3: obj.DMAS();
				break;
		default: std::cout << "Try again\n";
	}
	return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */
