/*
 * =====================================================================================
 *
 *       Filename:  LA_calculator.cpp
 *
 *    Description: General purpose Linear algebra calculator
 *
 *        Version:  1.0
 *        Created:  09/13/2021 01:38:50 PM
 *       Revision:  none
 *       Compiler:  g++
 *
 *         Author:  Dhruva Gole (181030017), goledhruva@gmail.com
 *   Organization:  VJTI
 *
 * =====================================================================================
 */

#include "../include/LA_calculator.h"

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  Inverse
 *  Description:  Finds inverse of the entered matrix
 * =====================================================================================
 */
	int
Calculator::Inverse ()
{
	std::cout << "Enter the rows and cols of matrix you need inverse of: \n";
	std::cin >> r >> c;
	double element[r+c+2];
	std::cout << "Enter the matrix\n";

	for ( int i = 0; i < r; i += 1 ) {
		for ( int j = 0; j < c; j += 1 ) {
			std::cin >> element[i+j];
		}
	}
	Eigen::Map<Eigen::MatrixXd,0,Eigen::OuterStride<> >   M(element,r,c,Eigen::OuterStride<>(1));
	std::cout << "you entered:\n" << M << std::endl ;
	std::cout << "The inverse of that is:\n" << M.inverse() << std::endl;

	return EXIT_SUCCESS;
}		/* -----  end of function Calculator::Inverse  ----- */


/*
 * ===  FUNCTION  ======================================================================
 *         Name:  Determinant
 *  Description:  asks for matrix and it's dimensions, then finds it's determinant
 * =====================================================================================
 */
	int
Calculator::Determinant ()
{
	printf("Enter the dimension of your square matrix:\n");
	std::cin >> r;
	printf("Enter the Matrix\n");
	return EXIT_SUCCESS;
}		/* -----  end of function Determinant  ----- */


/*
 * ===  FUNCTION  ======================================================================
 *         Name:  Calculator::DMAS
 *  Description:  Does all 4 operations on the input matrices
 * =====================================================================================
 */
	int
Calculator::DMAS ()
{
	std::cout << "Almost TODO\n";
	return EXIT_SUCCESS;
}		/* -----  end of function Calculator::DMAS  ----- */
